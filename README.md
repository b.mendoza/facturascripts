# FacturaScripts
Software de facturación y contabilidad para pymes, fácil, libre y con actualizaciones constantes. Es compatible con FacturaLux, Abanq y Eneboo. Es software libre bajo licencia GNU/LGPL.
 

## Acerca de FACTURALATAM

Estamos enfocados en crear herramientas y soluciones para desarrolladores de software, empresas medianas y pequeñas, tenemos un sueño, queremos democratizar la facturación electrónica.

## Instalación, configuración e integración de FacturaScripts con la API (Facturación electrónica DIAN) 

Para conocer el proceso de instalación, configuración e integración, visite la [documentación](https://docs.google.com/document/d/1WzdR6WtV8xuoKPPPiIPW4bwwErWzAER6186MmUXhCV0 "Clic")
 
 
## DEMO

Visite el siguiente enlace: [FacturaScripts](http://facturascripts.apifacturacolombia.com/ "Clic") 

Usuario:admin - 
Contraseña: 123456

## Patrocinadores FacturaScripts

 - **[FACTURALATAM](http://facturalatam.com/)** 
 - **[CursosDev](http://cursosdev.com/)** 
 

## Vulnerabilidades de seguridad

Si descubre una vulnerabilidad de seguridad en la API, envíe un correo electrónico a facturalatamglobal@gmail.com . Las vulnerabilidades de seguridad serán tratadas con prontitud.



