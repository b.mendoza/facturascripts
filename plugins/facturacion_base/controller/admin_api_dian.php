<?php

class admin_api_dian extends fs_controller
{
    public $dataApi;
    
    public function __construct() {
        parent::__construct(__CLASS__, 'Api Dian', 'admin', TRUE, TRUE);
    }
    
    protected function private_core() {
        if (isset($_POST['update'])) {
            if (is_object($response = json_decode($this->setConfig()))) {
                if ((property_exists($response, 'errors')) || (property_exists($response, 'message'))) {
                    foreach ($response->errors ?? [] as $key => $error) {
                        $this->new_error_msg($error[0]);
                    }
                    
                    if (property_exists($response, 'message')) {
                        $this->new_error_msg($response->message);
                    }
                }
                else {
                    $this->dataApi = $response;
                    
                    $this->new_message('Configuración guardada correctamente.');
                }
            }
        }
        
        if ($this->dataApi == null) $this->dataApi = json_decode($this->getConfig());
        
        if (!is_object($this->dataApi)) $this->new_error_msg('No se logro cargar los datos del API.');
        
        if ((is_array($this->dataApi)) && (isset($this->dataApi['message']))) $this->new_error_msg($this->dataApi['message']);
    }
    
    protected function getConfig() {
        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL => 'http://apirestdian.oo:8084/api/configurations/get',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer YxzC4qtBM99fSywuJgC38ahwh2ttX1SS8wTCDWusfnp8ia2qnQLnfrYVoLbT",
                "Content-Type: application/json",
                "cache-control: no-cache"
            ],
        ]);
        
        $response = curl_exec($curl);
        
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) return null;
        
        return $response;
    }
    
    public function setConfig() {
        $data = [];
        
        $data['company']['nit'] = $_POST['nit'];
        $data['company']['name'] = $_POST['name'];
        $data['company']['registration_name'] = $_POST['registration_name'];
        $data['company']['username'] = $_POST['username'];
        $data['company']['password'] = $_POST['password'];
        $data['company']['department'] = $_POST['department'];
        $data['company']['city'] = $_POST['city'];
        $data['company']['address'] = $_POST['address'];
        $data['company']['tax_level_code'] = $_POST['tax_level_code'];
        $data['company']['country_identification_code'] = $_POST['country_identification_code'];
        $data['company']['email'] = $_POST['email'];
        $data['company']['phone'] = $_POST['phone'];
        $data['company']['regime'] = $_POST['regime'];
        
        if (is_uploaded_file($_FILES['certificate']['tmp_name'])) {
            $data['certificate']['name'] = $_FILES["certificate"]['name'];
            $data['certificate']['base64'] = base64_encode(file_get_contents($_FILES["certificate"]['tmp_name']));
            $data['certificate']['password'] = $_POST['password_certificate'];
        }
        
        $data['configuration']['invoice_authorization'] = $_POST['invoice_authorization'];
        $data['configuration']['start_date'] = $_POST['start_date'];
        $data['configuration']['end_date'] = $_POST['end_date'];
        $data['configuration']['prefix'] = $_POST['prefix'];
        $data['configuration']['from'] = $_POST['from'];
        $data['configuration']['to'] = $_POST['to'];
        $data['configuration']['prefix_credit'] = $_POST['prefix_credit'];
        $data['configuration']['credit_to'] = $_POST['credit_to'];
        $data['configuration']['prefix_debit'] = $_POST['prefix_debit'];
        $data['configuration']['debit_to'] = $_POST['debit_to'];
        $data['configuration']['provider_id'] = $_POST['provider_id'];
        $data['configuration']['software_id'] = $_POST['software_id'];
        $data['configuration']['technical_key'] = $_POST['technical_key'];
        $data['configuration']['software_security_code'] = $_POST['software_security_code'];
        $data['configuration']['configuration_type_id'] = $_POST['configuration_type_id'];
        
        $curl = curl_init();
        
        curl_setopt_array($curl, [
            CURLOPT_URL => 'http://apirestdian.oo:8084/api/configurations',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer YxzC4qtBM99fSywuJgC38ahwh2ttX1SS8wTCDWusfnp8ia2qnQLnfrYVoLbT",
                "Content-Type: application/json",
                "cache-control: no-cache"
            ],
        ]);
        
        $response = curl_exec($curl);
        
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) return null;
        
        return $response;
    }
}
